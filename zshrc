#!/bin/zsh

# Inherit logon environment
source ~/.profile

export ZSH="$HOME/.config/zsh"


for config in $ZSH/*.zsh; do
    source $config
done
