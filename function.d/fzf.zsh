#!/bin/zsh

if [[ -d /usr/share/fzf ]]; then
    ENHANCD_FILTER="fzf"
    FZF_DEFAULT_OPTS='--reverse --inline-info --prompt="[fzf] » "
        --color fg:252,bg:233,hl:67,fg+:252,bg+:235,hl+:81
        --color info:144,prompt:161,spinner:135,pointer:135,marker:118'
    FZF_TMUX=0
    FZF_DEFAULT_COMMAND="ls -R | grep \":$\" | sed -e 's/:$//' -e 's/^/   /' -e 's/-/|/'"
    export ENHANCD_FILTER FZF_DEFAULT_OPTS FZF_TMUX FZF_DEFAULT_COMMAND

    source /usr/share/fzf/completion.zsh
    source /usr/share/fzf/key-bindings.zsh
fi
