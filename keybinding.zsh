#!/bin/zsh

# Enable edit commandline in editor
autoload -U edit-command-line
zle -N edit-command-line

bindkey '^V' edit-command-line


# Enable home/end/ins/del/... keys
# create a zkbd compatible hash
# to add other keys to this hash, see: man 5 terminfo
typeset -A key

key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key accordingly
[[ -n "${key[Home]}"     ]]  && bindkey  "${key[Home]}"     beginning-of-line
[[ -n "${key[End]}"      ]]  && bindkey  "${key[End]}"      end-of-line
[[ -n "${key[Insert]}"   ]]  && bindkey  "${key[Insert]}"   overwrite-mode
[[ -n "${key[Delete]}"   ]]  && bindkey  "${key[Delete]}"   delete-char
[[ -n "${key[Up]}"       ]]  && bindkey  "${key[Up]}"       up-line-or-history
[[ -n "${key[Down]}"     ]]  && bindkey  "${key[Down]}"     down-line-or-history
[[ -n "${key[Left]}"     ]]  && bindkey  "${key[Left]}"     backward-char
[[ -n "${key[Right]}"    ]]  && bindkey  "${key[Right]}"    forward-char
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"   beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}" end-of-buffer-or-history

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        printf '%s' "${terminfo[smkx]}"
    }
    function zle-line-finish () {
        printf '%s' "${terminfo[rmkx]}"
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

if [[ $UID -eq 0 ]]; then
    #pass
else

    # binds double `esc` to append sudo
    sudo-command-line() {
        [[ -z $BUFFER ]] && zle up-history
        if [[ $BUFFER == sudo\ * ]]; then
            LBUFFER="${LBUFFER#sudo }"
        else
            LBUFFER="sudo $LBUFFER"
        fi
    }

    zle -N sudo-command-line
    # Defined shortcut keys: [Esc] [Esc]
    bindkey "\e\e" sudo-command-line

    # Bind a ncurses application to a keystoke, but it will not accept interaction.
    _ncmpcpp_show()  {BUFFER="ncmpcpp";      zle accept-line;}
    # _ranger_show()   {BUFFER="ranger";       zle accept-line;}
    _htop_show()     {BUFFER="htop";         zle accept-line;}
    # _glances_show()  {BUFFER="glances";      zle accept-line;}
    # _ncdu_show()     {BUFFER="ncdu --si";    zle accept-line;}

    _cd_undo_key()   {popd > /dev/null;      zle reset-prompt; echo;}
    _cd_parent_key() {pushd .. > /dev/null;  zle reset-prompt; echo;}

    zle -N _ncmpcpp_show
    # zle -N _ranger_show
    zle -N _htop_show
    # zle -N _glances_show
    # zle -N _ncdu_show
    zle -N _cd_parent_key
    zle -N _cd_undo_key

    bindkey '^[\' _ncmpcpp_show # alt + \
    # bindkey '^[]' _ranger_show  # alt + ]
    # bindkey '^[[' _ncdu_show    # alt + [
    # bindkey '^[=' _glances_show # alt + =
    bindkey '^[=' _htop_show     # alt + =
    bindkey '^[k' _cd_parent_key # alt + k
    bindkey '^[h' _cd_undo_key   # alt + h
fi
