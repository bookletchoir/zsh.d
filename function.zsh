#!/bin/zsh

if [ -d "$ZSH/function.d" ]; then
    for file in $ZSH/function.d/*.zsh; do
        source "$file"
    done
fi

