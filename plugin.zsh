#!/bin/zsh

# dircolors
eval $(dircolors $ZSH/plugin.d/dircolors-solarized/dircolors.ansi-dark)
#eval $(dircolors $ZSH/plugin.d/LS_COLORS/LS_COLORS)
#eval $(dircolors $ZSH/plugin.d/dircolors-monokai)

# bold the directory color
LS_COLORS=$LS_COLORS:'di=1;36:'

# Colorize completion output
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}


# extra completions
source "$ZSH/plugin.d/zsh-completions/zsh-completions.plugin.zsh"


# syntax highlight
source "$ZSH/plugin.d/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=043,bold'
ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=208,bold'

ZSH_HIGHLIGHT_STYLES[alias]='fg=086,bold'
ZSH_HIGHLIGHT_STYLES[function]='fg=086,bold'

ZSH_HIGHLIGHT_STYLES[builtin]='fg=cyan,bold'
ZSH_HIGHLIGHT_STYLES[command]='fg=cyan,bold'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=magenta,bold'

ZSH_HIGHLIGHT_STYLES[path]='fg=043,bold'
ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=198,bold'

ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='fg=135'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='fg=135,bold'

ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=220'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=yellow,bold'

ZSH_HIGHLIGHT_STYLES[dollar-quoted-option]='fg=210,bold'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-option]='fg=210,bold'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=208,bold'

ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=cyan,bg=black'
ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]='fg=cyan,bg=black'

ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=198,bold'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=cyan,bold'
ZSH_HIGHLIGHT_STYLES[assign]='fg=144,bold'
ZSH_HIGHLIGHT_STYLES[redirection]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[comment]='fg=008'

ZSH_HIGHLIGHT_STYLES[bracket-error]='fg=198,bold'

