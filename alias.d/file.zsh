#!/bin/zsh

# Alias for search functions

alias fd='find . -type d -name'
alias ff='find . -type f -name'

