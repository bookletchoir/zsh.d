#!/bin/zsh

alias ls='env LC_COLLATE=C ls --color --group-directories-first'
alias l='env LC_COLLATE=C ls -lFh --color --group-directories-first'
alias la='env LC_COLLATE=C ls -lAFh --color --group-directories-first'
alias lr='env LC_COLLATE=C ls -tRFh --color --group-directories-first'
alias lt='env LC_COLLATE=C ls -ltFh --color --group-directories-first'
alias ll='env LC_COLLATE=C ls -l --color --group-directories-first'
alias ldot='env LC_COLLATE=C ls -ld .* --color --group-directories-first'
alias lS='env LC_COLLATE=C ls -1FSsh --color --group-directories-first'
alias lart='env LC_COLLATE=C ls -1Fcart --color --group-directories-first'
alias lrt='env LC_COLLATE=C ls -1Fcrt --color --group-directories-first'
