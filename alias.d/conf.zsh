#!/bin/zsh

# Aliases for common config files

alias tmuxrc="nvim ~/.tmux.conf"
alias nvimrc="nvim ~/.config/nvim/init.vim"
alias i3conf="nvim ~/.config/i3/config"
alias i3barconf="nvim ~/.config/i3/i3blocks.conf"
alias startupconf="nvim ~/.config/autostartrc"
