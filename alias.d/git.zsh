#!/bin/zsh

# Aliases for git

alias ga="git add"
alias gaa="git add --all"

alias gco="git checkout"

alias gp="git push"
alias gl="git pull"
