#!/bin/zsh

# Custom aliases set

alias sketchlog="journalctl -e -b -o short $@"
alias sketchpkg="less -c +G $@ $HOME/.local/pacman/pacman.log"

